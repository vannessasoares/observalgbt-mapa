FactoryGirl.define do
	factory :valid_atividade, class: Atividade do
		nome "Movimentos Sociais"
		descricao "Movimentos Sociais etc"
	end

	factory :invalid_atividade, class: Atividade do
		nome nil
		descricao nil
	end
end