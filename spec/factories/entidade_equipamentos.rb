FactoryGirl.define do
	factory :valid_entidade, class: EntidadeEquipamento do
		instituicao "MyString"
		nome "MyString"
		descricao "MyString"
		email "mystring@mystring.com"
		contato "MyString"
		apresenta "MyString"
		site "MyString"
		cep "72871023"
		logradouro "MyString"
		complemento "MyString"
		estado "São Paulo"
		cidade "São Paulo"
		publicar true
	end

	factory :invalid_entidade, class: EntidadeEquipamento do
		instituicao nil
		nome nil
		descricao nil
		email nil
		contato nil
		apresenta nil
		site nil
		cep nil
		logradouro nil
		complemento nil
		estado nil
		cidade nil
		publicar true
	end
end