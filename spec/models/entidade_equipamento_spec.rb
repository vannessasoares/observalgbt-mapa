require 'rails_helper'

RSpec.describe EntidadeEquipamento, type: :model do
	subject(:valid_entidade){
		FactoryGirl.build(:valid_entidade)
	}

	subject(:invalid_entidade){
		FactoryGirl.build(:invalid_entidade)
	}

	describe "#create" do
		it "when providing valid data" do
			expect(valid_entidade).to be_valid
		end

		it "when providing invalid data" do
			expect(invalid_entidade).not_to be_valid
		end

		it "not to be null" do
			expect(invalid_entidade).not_to be_nil
		end

	end
end