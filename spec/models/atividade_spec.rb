require 'rails_helper'

RSpec.describe Atividade, type: :model do
	subject(:valid_atividade){
		FactoryGirl.build(:valid_atividade)
	}

	subject(:invalid_atividade){
		FactoryGirl.build(:invalid_atividade)
	}

	describe "#create" do
		it "when providing valid data" do
			expect(valid_atividade).to be_valid
		end

		it "when providing invalid data" do
			expect(invalid_atividade).not_to be_valid
		end

		it "not to be null" do
			expect(valid_atividade).not_to be_nil
		end

	end
end