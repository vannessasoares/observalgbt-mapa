#installing ruby with rbenvcd
sudo chmod -R 777 /opt/rbenv/
sudo chmod -R 777 /opt/rbenv/versions/
sudo chmod -R 777 /opt/rbenv/shims/
rbenv install 2.3.1
rbenv global 2.3.1
echo "gem: --no-document" > ~/.gemrc

#installing gems
sudo chown -R "$USER" /opt/rbenv
sudo chmod -R 777 /opt/rbenv/versions/2.3.1/lib/ruby/gems/2.3.0
sudo apt install libmagick++-dev -y
sudo apt install libsqlite3-dev
sudo apt install libcurl3 libcurl3-gnutls libcurl4-openssl-dev -y
#gem install curb -v '0.8.8'
gem install bundler
cd /vagrant
bundle install
